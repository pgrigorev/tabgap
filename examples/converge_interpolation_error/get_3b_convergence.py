from tabulate.tabgap import tabGAP
from tabulate.utils import get_interpolation_error

# Create tabGAPs with increasingly dense 3b grids to
# check convergence of interpolation error.
# Typically, with a cutoff <= 5 Å, a (n, n, n) grid
# of at least n >= 50 is needed. This example shows
# that for n=50, the interpolation error is ~0.1 meV/atom
# and ~ 0.015 eV/Å.
# NOTE this script will take a while to run... (30min+)

gap_xml = '../test_files/gap_W-Ta_example_2b+3b.xml'
xyz_file = '../test_files/db_W-Ta_example_no_isolated.xyz'

tabgap = tabGAP(gap_xml, n2b=2000)

for n in [10, 20, 30, 40, 50, 60]:
    print(f'# computing n3b=({n}, {n}, {n})')
    tabgap.set_n3b((n, n, n))
    tabgap.compute_energies(ncores=3, save_data=False)
    tabgap.write_potential_files()
    rmse = get_interpolation_error(xyz_file, tabgap.pair_style,
                                   tabgap.pair_coeff, gap_xml=gap_xml,
                                   compute_gap=True)
    print('# n, E (meV/atom), F (eV/Å)')
    print(n, rmse['energy'], rmse['force'], flush=True)
