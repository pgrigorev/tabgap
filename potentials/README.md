Please cite the correct reference if you use any of these tabGAPs:

**Mo-Nb-Ta-V-W** version 1 (2b+3b): J. Byggmästar, K. Nordlund, F. Djurabekova, Modeling refractory high-entropy alloys with efficient machine-learned interatomic potentials: Defects and segregation, Phys. Rev. B 104, 104101 (2021), https://doi.org/10.1103/PhysRevB.104.104101, https://arxiv.org/abs/2106.03369

**Mo-Nb-Ta-V-W** version 2 (2b+3b+EAM): J. Byggmästar, K. Nordlund, and F. Djurabekova, Simple machine-learned interatomic potentials for complex alloys, https://arxiv.org/abs/2203.08458

**Fe**: J. Byggmästar, G. Nikoulis, A. Fellman, F. Granberg, F. Djurabekova, K. Nordlund, Multiscale machine-learning interatomic potentials for ferromagnetic and liquid iron, J. Phys.: Condens. Matter 34 305402 (2022), https://arxiv.org/abs/2201.10237, https://doi.org/10.1088/1361-648X/ac6f39
